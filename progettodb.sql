Drop database IF EXISTS EPOOL;
CREATE DATABASE EPOOL;
USE EPOOL;

SET autocommit = 0;

#Creazione tabelle

#tabella UTENTE
CREATE TABLE UTENTE (

	Email varchar(30) primary key  check (Email like '%@%.%'),
    PasswordEm varchar(12) not null,
    dataNascita date not null,
    luogoNascita varchar(30) not null,
    Nome varchar(15) not null,
    Cognome varchar(15) not null
	) ENGINE = INNODB;

#Popolamento Utente
INSERT INTO UTENTE (Email,PasswordEm, dataNascita, luogoNascita, Nome, Cognome)
values
	('enrico.morselli@fake.com','1234','1997-04-05','Bologna','Enrico','Morselli'),
	('marco.aspromonte@fake.com','1234','1997-10-30','Foggia','Marco','Aspromonte'),
	('thomas.lodi@fake.com','1234','1996-07-08','Bologna','Thomas','Lodi'),
	('mario.rossi@fake.com','1234','1987-09-19','Parma','Mario','Rossi'),
	('luigi.bianchi@fake.com','1234', '1991-02-10','Milano','Luigi','Bianchi');


#Creazione tabella foto
CREATE TABLE FOTO (
	CodFoto int auto_increment primary key,
    EmailUtente varchar(30) check (Email like '%@%.%'),
    immagine varchar(100) NOT NULL,
    FOREIGN KEY(EmailUtente) references UTENTE(Email) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE = INNODB;

    insert into FOTO (emailUtente,immagine) values
		('enrico.morselli@fake.com','foto1.jpg'),
        ('enrico.morselli@fake.com','foto2.jpg'),
        ('enrico.morselli@fake.com','foto3.jpg'),
        ('marco.aspromonte@fake.com','foto4.jpg'),
        ('marco.aspromonte@fake.com','foto5.jpg'),
        ('thomas.lodi@fake.com','foto6.jpg'),
        ('thomas.lodi@fake.com','foto7.jpg');

#Creazione tabella AREASOSTA
CREATE TABLE AREASOSTA(
	Latitudine float (10,6) not null,
    Longitudine float (10,6) not null,
    Indirizzo varchar(50) not null,
    StatoColonnine bool,
    primary key(Indirizzo)
    ) ENGINE = INNODB;

    #Popolamento AREASOSTA
    INSERT INTO AREASOSTA (Latitudine, Longitudine, Indirizzo, StatoColonnine)
    values
		('44.5009984','11.344679','Via Irnerio 1',FALSE),
		('44.495197','11.3425492','Via Dell Indipendenza 2',TRUE),
		('44.493396','11.348367','Strada Maggiore 15',FALSE),
		('44.5042199','11.3414977','Via Cesare Boldrini 6',TRUE),
		('44.4949308','11.3428606','Via Francesco Rizzoli 2',TRUE),
		('44.4964882','11.3412688','Via Parigi 4',FALSE),
		('44.49736','11.345159','Via Marsala 15',TRUE),
		('44.50181','11.349242','Via Finelli 3',TRUE),
		('44.5042144','11.3442351','Stazione Centrale',FALSE);


#Creazione tabella VEICOLO
CREATE TABLE VEICOLO(
	Targa varchar(9) primary key,#da sistemare
    Modello varchar(20),
    Capienza smallint not null,
    Descrizione varchar(100),
    TariffaFeriale smallint not null,
    TariffaFestiva smallint not null,
    Societa varchar(20) references SOCIETA(Nome)
    ) ENGINE = INNODB;

    #Popolamento veicolo
	INSERT INTO VEICOLO (Targa, Modello, Capienza, Descrizione, TariffaFeriale, TariffaFestiva, Societa)
    values
    ('MA 292 CE', 'Audi e-tron','4','Buona Tenuta','40','18','Tuacar'),
    ('RO 782 TS', 'Mini Cooper E','4','Comoda e Veersatile','40','18','Miacar'),
    ('ML 593 NE', 'Mercedes EQ C','5','Veloce','40','18','Tuacar'),
    ('VA 493 GM', 'Tesla Model Y','4','Ottima tenuta','40','18','Miacar'),
    ('BR 291 MS', 'Volkswagen e-Golf','5','Contuma Poco','40','18','Tuacar'),
    ('NU 275 HG', '	Tesla Model S','4','Ottimo veicolo','40','42','Tuacar');

#Creazione tabella SOCIETA
CREATE TABLE SOCIETA(
	CodSocieta int primary key auto_increment,
    Nome varchar(30) not null,
    NumVeicoli smallint,#(RIDONDANZA)
    Telefono varchar(10) not null,
    URLsito varchar(100) check (URLsito like 'www.%.%')
    ) ENGINE = INNODB;

    #Popolamento societa
    INSERT INTO SOCIETA(Nome, NumVeicoli, Telefono, URLsito)
    values
    ('Tuacar','4','051 922828','www.tuacar.com'),
    ('Miacar','2','051 837849','www.miacar.com');


#Creazione tabella PARCHEGGIO
CREATE TABLE PARCHEGGIO(
	TargaVeicolo varchar(9) references VEICOLO(Targa),
	Indirizzo varchar(30) not null,
    PRIMARY KEY(TargaVeicolo,Indirizzo),
	FOREIGN KEY(Indirizzo) references AREASOSTA(Indirizzo)
    ) ENGINE = INNODB;

    #popolamento parcheggio
    insert into PARCHEGGIO(targaveicolo,indirizzo)
		values
		("MA 292 CE","Via Marsala 15"),
		("RO 782 TS", "Strada Maggiore 15"),
    ("ML 593 NE", "Via Dell Indipendenza 2"),
    ("VA 493 GM", "Via Cesare Boldrini 6"),
    ("BR 291 MS", "Via Francesco Rizzoli 2"),
		("NU 275 HG", "Via Parigi 4");


#Creazione tabella PRENOTAZIONE
CREATE TABLE PRENOTAZIONE(
	  CodPrenotazione smallint auto_increment primary key,
    CampoNote varchar(200),
    OraInizio datetime not null,
    OraFine datetime,
    IndirizzoPartenza varchar(50) not null,
    IndirizzoArrivo varchar(50) not null,
    EmailUtente varchar(30) check (EmailUtente like '%@%.%'),
    TargaVeicolo varchar(9) not null,
    Durata double(3,2),
    Costo double(3,2),
    FOREIGN KEY(IndirizzoArrivo) references AREASOSTA(Indirizzo),
		FOREIGN KEY(IndirizzoPartenza) references AREASOSTA(Indirizzo),
    FOREIGN KEY(EmailUtente) references UTENTE(Email),
    FOREIGN KEY(TargaVeicolo) references VEICOLO(Targa)
) ENGINE = INNODB;

#Creazione tabella BROCHURE
CREATE TABLE BROCHURE(
	CodBrochure smallint auto_increment primary key,
	NomeBrochure varchar(10),
	NomeSocieta varchar(30) references SOCIETA(Nome)
) ENGINE = INNODB;

INSERT INTO BROCHURE(NomeBrochure,NomeSocieta) values ("Miacar.pdf","Miacar"),("Tuacar.pdf","Tuacar");

#Creazione tabella FEEDBACK
CREATE TABLE FEEDBACK(
	CodFeedback smallint auto_increment primary key,
    Titolo varchar(20) not null, #Inserimento automatico del titolo
    Testo varchar(200) not null,
    DataInserimento Date not null,
    TargaVeicolo varchar(9) references VEICOLO(Targa)
    ) ENGINE = INNODB;

#Creazione della tabella FEEDBACKUT
CREATE TABLE FEEDUTENTE(
	CodFeedback SMALLINT auto_increment,
	EmailUt varchar(30),
	PRIMARY KEY (CodFeedback, EmailUt),
	FOREIGN KEY (CodFeedback) REFERENCES FEEDBACK(CodFeedback),
	FOREIGN KEY (EmailUt) REFERENCES UTENTE (Email)
) ENGINE = INNODB;

#Creazione tabella Azienda
CREATE TABLE AZIENDA(
    Nome varchar(20)  primary key,
    Indirizzo varchar(20) not null,
    Telefono varchar(20),
    TelResponsabile varchar(20) not null
    ) ENGINE = INNODB;

#Popolamento tabella Azienda
INSERT INTO AZIENDA(Nome, Indirizzo, Telefono, TelResponsabile) VALUES
	("Morselli s.r.l.", "Via del Francia 14", "0511494333", "3311332829"),
	("Susta s.r.l","Via L. Savioli 9", "0512992335", "3512967979");

#Creazione tabella Tragitto
CREATE TABLE TRAGITTO(
		CodTragitto smallint auto_increment primary key,
    Tipologia ENUM('URBANO','EXTRAURBANO','AUTOSTRADALE','MISTO'),
    KmPercorso smallint
    ) ENGINE = INNODB;

#creazione tabella TAPPA
CREATE TABLE TAPPA(
		CodTappa smallint auto_increment primary key,
    Via varchar(50) not null,
		Citta varchar(30) not null,
    LatTappa float (10,6) not null,
    LongTappa float (10,6) not null,
    OraStimata time
    ) ENGINE = INNODB;

/*creazione tabella UTSEMPLICE*/
CREATE TABLE UTSEMPLICE(
	EmailUtS varchar(30) primary key  check (EmailUtS like '%@%.%'),
    FOREIGN KEY(EmailUtS) REFERENCES UTENTE(Email)
	) ENGINE = INNODB;

/*popolamento UTSEMPLICE*/
INSERT INTO UTSEMPLICE(EmailUtS) VALUES
	('mario.rossi@fake.com'),
	('thomas.lodi@fake.com');

/*creazione tabella UTPREMIUM*/
CREATE TABLE UTPREMIUM(
	EmailUtP varchar(30) primary key  check (EmailUtP like '%@%.%'),
	FOREIGN KEY(EmailUtP) REFERENCES UTENTE(Email)
    ) ENGINE = INNODB;

/*creazione tabella UTDIPENDENTE*/
CREATE TABLE UTDIPENDENTE(
	EmailUtD varchar(30) primary key  check (Email like '%@%.%'),
	FOREIGN KEY(EmailUtD) REFERENCES UTENTE(Email),
	NomeAzienda varchar(20) REFERENCES AZIENDA(Nome)
	) ENGINE = INNODB;

/*popolamento UTDIPENDENTE*/
INSERT INTO UTDIPENDENTE(EmailUtD, NomeAzienda) VALUES
 ("enrico.morselli@fake.com", "Mors s.r.l."),
 ("luigi.bianchi@fake.com", "Mors s.r.l."),
 ("marco.aspromonte@fake.com", "Susta s.r.l.");

 #creazione tabella VALUTAZIONE
 CREATE TABLE VALUTAZIONE(
 		UtenteValutatore varchar(30) check (UtenteValutatore like '%@%.%'),
 		UtenteValutato varchar(30) check (UtenteValutato like '%@%.%'),
     CodTragitto smallint,
     Testo varchar(500),
     DataTesto Date,
     Voto smallint check(Voto between 1 and 5 ),
     PRIMARY KEY(UtenteValutatore, UtenteValutato, CodTragitto),
	 FOREIGN KEY(UtenteValutatore)  references UTENTE(Email),
     foreign key(UtenteValutato) references UTENTE(Email),
     foreign key(CodTragitto) references TRAGITTO(CodTragitto)
     ) ENGINE = INNODB;

#creazone tabella SOCPUBBLICA
CREATE TABLE SOCPUBBLICA(
	CodPubblica int REFERENCES SOCIETA(CodSocieta)
	) ENGINE = INNODB;

#creazione tabella SOCPRIVATA
CREATE TABLE SOCPRIVATA(
	CodPrivata int REFERENCES SOCIETA(CodSocieta)
	) ENGINE = INNODB;

#creazione tabella FEEDSOC
CREATE TABLE FEEDSOC(
	CodSocieta int,
    CodFeed smallint,
    FOREIGN KEY(CodSocieta) REFERENCES SOCIETA(CodSocieta),
	FOREIGN KEY(CodFeed) REFERENCES FEEDBACK(CodFeedback)
    ) ENGINE = INNODB;

#creazione tabella CARPOOLPREM
CREATE TABLE CARPOOLPREM(
		EmailUtente varchar(30) check (Email like '%@%.%'),
		CodTragitto smallint,
		TappaPartenza VARCHAR(50) not null,
		TappaArrivo VARCHAR(50) not null,
	  PRIMARY KEY(CodTragitto,EmailUtente),
    FOREIGN KEY(CodTragitto) REFERENCES TRAGITTO(CodTragitto),
    FOREIGN KEY(EmailUtente) REFERENCES UTENTE(Email),
		FOREIGN KEY(TappaPartenza) REFERENCES PRENOTAZIONE(IndirizzoPartenza),
		FOREIGN KEY(TappaArrivo) REFERENCES PRENOTAZIONE(IndirizzoArrivo)
    ) ENGINE = INNODB;

#creazione tabella CARPOOLDIP
CREATE TABLE CARPOOLDIP(
		EmailUtente varchar(30) check (Email like '%@%.%'),
		CodTragitto smallint,
		TappaPartenza VARCHAR(50) not null,
		TappaArrivo VARCHAR(50) not null,
    PRIMARY KEY(CodTragitto,EmailUtente),
    FOREIGN KEY(CodTragitto) REFERENCES TRAGITTO(CodTragitto),
    FOREIGN KEY(EmailUtente) REFERENCES UTENTE(Email),
		FOREIGN KEY(TappaPartenza) REFERENCES PRENOTAZIONE(IndirizzoPartenza),
		FOREIGN KEY(TappaArrivo) REFERENCES PRENOTAZIONE(IndirizzoArrivo)
    ) ENGINE = INNODB;

#creazione tabella TRAGDIP
CREATE TABLE TRAGDIP(
    EmailUtente varchar(30) check (Email like '%@%.%'),
		CodTragitto smallint,
		TappaPartenza smallint not null,
		TappaArrivo smallint not null,
    PRIMARY KEY(CodTragitto,EmailUtente),
    FOREIGN KEY(CodTragitto) REFERENCES TRAGITTO(CodTragitto),
    FOREIGN KEY(EmailUtente) REFERENCES UTENTE(Email),
		FOREIGN KEY(TappaPartenza) REFERENCES TAPPA(CodTappa),
		FOREIGN KEY(TappaArrivo) REFERENCES TAPPA(CodTappa)
    ) ENGINE = INNODB;

/*creazione tabella TRAGPREM*/
CREATE TABLE TRAGPREM(
    EmailUtente varchar(30) check (Email like '%@%.%'),
		CodTragitto smallint,
		TappaPartenza smallint not null,
		TappaArrivo smallint not null,
    PRIMARY KEY(CodTragitto,EmailUtente),
    FOREIGN KEY(CodTragitto) REFERENCES TRAGITTO(CodTragitto),
    FOREIGN KEY(EmailUtente) REFERENCES UTENTE(Email),
		FOREIGN KEY(TappaPartenza) REFERENCES TAPPA(CodTappa),
		FOREIGN KEY(TappaArrivo) REFERENCES TAPPA(CodTappa)
    ) ENGINE = INNODB;

/*creazione tabella LISTA*/
CREATE TABLE LISTA(
	 CodTragitto smallint,
	 CodTappa smallint,
	 Ordine smallint,
    PRIMARY KEY(CodTragitto,CodTappa),
    FOREIGN KEY(CodTragitto) REFERENCES TRAGITTO(CodTragitto),
    FOREIGN KEY(CodTappa) REFERENCES TAPPA(CodTappa)
    ) ENGINE = INNODB;

/*creazione tabella PRENOTAZIONECARPOOL*/
CREATE TABLE PRENOTAZIONECARPOOL(
		CodTragitto smallint,
    CodPrenotazione smallint,
    PRIMARY KEY(CodTragitto,CodPrenotazione),
    FOREIGN KEY(CodTragitto) REFERENCES TRAGITTO(CodTragitto),
    FOREIGN KEY(CodPrenotazione) REFERENCES PRENOTAZIONE(CodPrenotazione)
    ) ENGINE = INNODB;

/*Struttura della tabella `messaggio`*/
CREATE TABLE MESSAGGIO (
  Id int primary key auto_increment,
  Titolo varchar(50) NOT NULL,
  emailMittente varchar(50) NOT NULL,
  emailDestinatario varchar(50) NOT NULL,
  Testo text,
  Data date DEFAULT NULL

) ENGINE=InnoDB DEFAULT CHARSET=latin1;
insert into MESSAGGIO (Titolo,emailMittente,emailDestinatario,Testo,Data)
values
("Viaggio di lavoro","enrico.morselli@fake.com","thomas.lodi@fake.com","Volevo ringraziarti per il passaggio, buona sertata.",current_date()),
("Viaggio di lavoro","thomas.lodi@fake.com","enrico.morselli@fake.com","Volevo ringraziarti per il passaggio, buona sertata.",current_date()),
("Viaggio di lavoro","enrico.morselli@fake.com","marco.aspromonte@fake.com","Volevo ringraziarti per il passaggio, buona sertata.",current_date()),
("Viaggio di lavoro","marco.aspromonte@fake.com","enrico.morselli@fake.com","Volevo ringraziarti per il passaggio, buona sertata.",current_date()),
("Viaggio di lavoro","marco.aspromonte@fake.com","thomas.lodi@fake.com","Volevo ringraziarti per il passaggio, buona sertata.",current_date()),
("Viaggio di lavoro","thomas.lodi@fake.com","marco.aspromonte@fake.com","Volevo ringraziarti per il passaggio, buona sertata.",current_date())
;

#Disattivata modalita autocommit
SET autocommit = 0;

#STORED PROCEDURES

#PROCEDURA PRINTF PER STAMPA A VIDEO
DELIMITER $
CREATE PROCEDURE printf (mytext TEXT)
BEGIN
	SELECT mytext AS '';
END;
$ DELIMITER ;

#Procedura registrazione azienda
DELIMITER |
START TRANSACTION;
CREATE PROCEDURE NuovaAzienda(IN NuovoNome varchar(20) ,
							 IN NuovoIndirizzo varchar(20),
							 IN NuovoTelefono varchar(20),
							 IN NuovoTelefonoResponsabile varchar(20))
BEGIN
    IF NOT EXISTS((SELECT Nome FROM AZIENDA WHERE(AZIENDA.Nome = NuovoNome))) THEN
    INSERT INTO AZIENDA(Nome, Indirizzo ,Telefono ,TelResponsabile)
								VALUES (NuovoNome, NuovoIndirizzo, NuovoTelefono,
										NuovoTelefonoResponsabile);

	END IF;
END;
Commit;

| DELIMITER ;


#PROCEDURA REGISTRAZIONE UTENTE

DELIMITER |
START TRANSACTION;
CREATE PROCEDURE NuovoUtente(IN NuovaEmail varchar(30),
							 IN NuovaPassword varchar(12),
							 IN NuovaDataNascita date,
							 IN NuovoLuogoNascita varchar(30),
							 IN NomeNuovoUtente varchar(15),
							 IN CognomeNuovoUtente varchar(15))
BEGIN
    IF NOT EXISTS((SELECT Email FROM UTENTE WHERE(UTENTE.Email = NuovaEmail))) THEN
    INSERT INTO UTENTE(Email, PasswordEm, DataNascita, LuogoNascita, Nome, Cognome)
								VALUES (NuovaEmail, NuovaPassword, NuovaDataNascita, NuovoLuogoNascita,
										NomeNuovoUtente, CognomeNuovoUtente);
    ELSE
	CALL printf ('UTENTE ATTUALMENTE REGISTRATO!');
    ROLLBACK;
	END IF;
END;
Commit;

| DELIMITER ;


#PROCEDURA DI AUTENTICAZIONE
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE AutenticazioneUtente(IN EmailN varchar(30), IN passwordE varchar(12))

BEGIN
	SELECT COUNT(*) AS counter
	FROM UTENTE
	WHERE (UTENTE.Email=EmailN) AND (UTENTE.passwordEm=passwordE);

END;
commit;
$ DELIMITER ;

#VISUALIZZA DATI UTENTI REGISTRATI AL SISTEMA
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE VisualizzaUtenteDati(IN EmailUtLoggato varchar(30))
BEGIN

	SELECT Email,dataNascita,luogoNascita,Nome,Cognome
	FROM UTENTE
	WHERE Email != EmailUtLoggato; #esclude l utente loggato dalla lista

END;
commit;
$ DELIMITER ;

/*VISUALIZZA DATI UTENTE REGISTRATO AL SISTEMA*/
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE VisualizzaDatiUtenteLog(IN EmailUtLoggato varchar(30))
BEGIN

	SELECT Email,dataNascita,luogoNascita,Nome,Cognome
	FROM UTENTE
	WHERE Email = EmailUtLoggato;

END;
commit;
$ DELIMITER ;

/*EFFETTUARE NUOVA PRENOTAZIONE*/
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE NuovaPrenotazione(IN NewCampoNote varchar(200),
                                   IN IndirizzoNewPartenza varchar(50),
                                   IN IndirizzoNewArrivo varchar (50),
								   						 		 IN NewEmail varchar(30),
                                   IN TargaNewVeicolo varchar(9))

BEGIN

INSERT INTO PRENOTAZIONE (CampoNote,OraInizio,OraFine,IndirizzoPartenza,IndirizzoArrivo,EmailUtente,TargaVeicolo)
							VALUES (NewCampoNote,current_timestamp(),NULL,IndirizzoNewPartenza,IndirizzoNewArrivo,NewEmail,TargaNewVeicolo);

#END IF;
END;
commit;
$ DELIMITER ;

/*INSERIMENTO DI UN NUOVO VEICOLO*/
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE NuovoVeicolo(IN NewTarga varchar(9), IN NewModello varchar(20), IN NewCapienza smallint,
							  IN NewDescrizione varchar(100), IN NewTariffaFeriale smallint,
                              IN NewTariffaFestiva smallint, IN NewSocieta varchar(20))

BEGIN
IF NOT EXISTS (select Targa from VEICOLO where Targa=NewTarga) then
INSERT INTO VEICOLO(Targa, Modello, Capienza,Descrizione, TariffaFeriale, TariffaFestiva, Societa)
					values (NewTarga, NewModello, NewCapienza, NewDescrizione, NewTariffaFeriale, NewTariffaFestiva, NewSocieta);

END IF;
END;
commit;
$ DELIMITER ;

/*INSERIMENTO DI UNA NUOVA AREA DI SOSTA*/
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE NuovaAreaSosta(IN NewLatitudine float (10,6), NewLongitudine float(10,6), IN NewIndirizzo varchar(30), NewStatoColonnine bool)

BEGIN
IF NOT EXISTS( select Latitudine, Longitudine from AREASOSTA where Latitudine=NewLatitudine AND Longitudine=NewLongitudine)
THEN
INSERT INTO AREASOSTA(Latitudine,Longitudine,Indirizzo,StatoColonnine) values(NewLatitudine, NewLongitudine, NewIndirizzo, NewStatoColonnine);

END IF;
END;
commit;
$ DELIMITER ;

/*VISUALIZZA LO STORICO DELLE PRENOTAZIONI*/
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE VisualizzaPrenotazioni()
BEGIN
	SELECT DataPrenotazione, OraInizio, OraFine, EmailUtente, TargaVeicolo FROM PRENOTAZIONE;
END;
commit;
$ DELIMITER ;

#VISUALIZZA DATI SOCIETÀ DI CAR-SHARING
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE VisualizzaDatiSoc ()
BEGIN
	SELECT * FROM SOCIETA;
END;
commit;
$ DELIMITER ;

#PROCEDURA PER NUOVO UTENTE DIPENDENTE
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE NuovoUtenteDipendente(IN NuovaEmail varchar(30),
							 IN NuovoNomeAzienda varchar(20))

  BEGIN
    INSERT INTO UTDIPENDENTE(EmailUTD,NomeAzienda) VALUES (NuovaEmail, NuovoNomeAzienda);

END;
Commit;
$ DELIMITER ;


#PROCEDURA PER NUOVO UTENTE SEMPLICE
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE NuovoUtenteSemplice(IN NuovaEmail varchar(30))

  BEGIN
    INSERT INTO UTSEMPLICE(EmailUts) VALUES (NuovaEmail);

END;
Commit;
$ DELIMITER ;

/*Trigger che aggiorna il tipo utente da semplice a premium*/
DELIMITER $
CREATE TRIGGER AggiornaUtentePremium
AFTER INSERT ON PRENOTAZIONE
FOR EACH ROW

BEGIN

IF ((SELECT COUNT(*) FROM PRENOTAZIONE WHERE EmailUtente = NEW.EmailUtente) = 3)
THEN IF ((SELECT COUNT(*) FROM UTDIPENDENTE WHERE EmailUtD = NEW.EmailUtente) = 0)
	THEN INSERT INTO UTPREMIUM (EmailUtP) VALUES (New.EmailUtente) ;

	END IF;
END IF;

IF((SELECT COUNT(*) FROM UTSEMPLICE, UTPREMIUM WHERE UTPREMIUM.EmailUtP=UTSEMPLICE.EmailUts ) = 1)then
Delete from UtSemplice where Emailuts=NEW.EmailUtente;
END IF;
Delete from PARCHEGGIO where TargaVeicolo=NEW.TargaVeicolo;

END;

$ DELIMITER ;

/*Controlla se l utente è registrato nel sistema. In caso contrario restituisce 0
#altrimenti restituisce 1, 2 o 3 in base al tipo di utente (premium o dipendente)*/
DELIMITER $
CREATE PROCEDURE `checkLogin` (IN EmailIn varchar(30), IN passwordE varchar(12), OUT `tipo` INT)
BEGIN

	IF((SELECT  COUNT(*) FROM UTENTE WHERE Email = EmailIn AND PasswordEm = passwordE) > 0) THEN
    	IF((SELECT COUNT(*) FROM UTDIPENDENTE WHERE EmailUtD = EmailIn) > 0 ) THEN
        	SET tipo = 1; #utente dipendente
        ELSEIF ((SELECT COUNT(*) FROM UTPREMIUM WHERE  EmailUtP = EmailIn) > 0) THEN
        	SET tipo = 2; #utente premium
        ELSEIF ((SELECT COUNT(*) FROM UTSEMPLICE WHERE  EmailUtS = EmailIn) > 0) THEN
        	SET tipo = 3;
        END IF;
   	ELSE
    	SET tipo = 0;
    END IF;

END
$ DELIMITER ;

#PROCEDURA INVIO MESSAGGIO
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE invioMessaggio ( IN NewTitolo VARCHAR(50), IN NewEmailMittente VARCHAR(50), IN NewEmailDestinatario VARCHAR(50),
									IN NewTesto TEXT, IN data DATE,  OUT `msg` INT)

BEGIN
	IF(SELECT COUNT(*) FROM UTENTE WHERE Email = NewEmailDestinatario) THEN
    	INSERT INTO messaggio(Titolo, emailMittente, emailDestinatario, Testo, Data) VALUES
											(NewTitolo, NewEmailMittente, NewEmailDestinatario, NewTesto, data);
        SET msg =1;

    ELSE
    	SET msg = 2;
    END IF;
    END;
    COMMIT;
$ DELIMITER ;

#PROCEDURA PER RICERCA AUTO PRESENTI AD UN DATO INDIRIZZO
DELIMITER $
START TRANSACTION;

CREATE  PROCEDURE listaAutoIndirizzo (IN IndirizzoIn varchar(30))  BEGIN

    SELECT TargaVeicolo, Modello
    FROM PARCHEGGIO, VEICOLO
    WHERE Indirizzo=IndirizzoIn and TargaVeicolo=Targa and TargaVeicolo NOT IN (SELECT TargaVeicolo FROM PRENOTAZIONE);
    END;
    COMMIT;
$ DELIMITER ;

DELIMITER $
START TRANSACTION;
CREATE PROCEDURE listaMessaggi (IN utenteIn VARCHAR(50))  BEGIN

    SELECT Id, emailMittente, emailDestinatario, Titolo, Testo, date_format(Data, '%d-%m-%y') as Data
    FROM messaggio
    WHERE emailMittente = utenteIn OR emailDestinatario = utenteIn;

END;
COMMIT;
$ DELIMITER ;

#PROCEDURA DI CONTROLLO INDIRIZZO DI ARRIVO
DELIMITER $
START TRANSACTION;
CREATE  PROCEDURE checkIndirizzoArr (IN IndirizzoInArrivo varchar(30),  OUT `ind` INT)  BEGIN

        IF((SELECT COUNT(*)
						FROM AREASOSTA
						WHERE Indirizzo=IndirizzoInArrivo) > 0) THEN SET ind = 1;
				ELSE SET ind = 0;
   			END IF;
END;
COMMIT;
$ DELIMITER ;

/*PROCEDURA PER MOSTRARE LE AUTO DISPONIBILI PER UNA PRENOTAZIONE*/
DELIMITER $
START TRANSACTION;

CREATE  PROCEDURE listaAutoDisponibili ()  BEGIN

    SELECT P.TargaVeicolo, P.Indirizzo, V.Modello, V.TariffaFeriale
    FROM PARCHEGGIO AS P, VEICOLO AS V
		WHERE P.TargaVeicolo = V.Targa;

    END;
    COMMIT;
$ DELIMITER ;

/*PROCEDURA PER SELEZIONARE UNA SINGOLA AUTOMOBILE DA PRENOTARE*/
DELIMITER $
START TRANSACTION;

CREATE PROCEDURE selectAuto (IN TargaVeicoloIn VARCHAR(9)) BEGIN

	SELECT V.Modello, P.Indirizzo
	FROM PARCHEGGIO AS P, VEICOLO AS V
	WHERE TargaVeicoloIn = V.Targa AND TargaVeicoloIn = P.TargaVeicolo;

	END;
	COMMIT;
$ DELIMITER ;

/*VISUALIZZA LE PRENOTAZIONI DI UN UTENTE*/
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE VisualizzaPrenotazioniUtente(IN EmailUtLoggato varchar(30))
#
BEGIN

	SELECT CampoNote,OraInizio,OraFine,IndirizzoPartenza,IndirizzoArrivo,TargaVeicolo, Durata, Costo
    FROM PRENOTAZIONE
    WHERE EmailUtente = EmailUtLoggato;

END;
commit;
$ DELIMITER ;

/*TERMINA PRENOTAZIONE*/
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE terminaPrenotazione(IN EmailUtLoggato VARCHAR(30), IN TargaVeicoloIn VARCHAR(9), IN IndirizzoArrivo varchar(50))

BEGIN

/*Seleziona il codice della prenotazione appena terminata*/
SET @CodPrenotazione = (SELECT CodPrenotazione FROM PRENOTAZIONE WHERE (EmailUtente = EmailUtLoggato) AND (OraFine IS NULL));

/*Verifica se sulla prenotazione terminata era stato abilitato il carpooling.
  poi ecupera il numero di utenti che avevano richiesto la condivisione del
	tragitto, siano essi utenti premium o dipendenti*/
SET @NumUtentiCP = (SELECT SUM(N) AS NumUtentiCP
										FROM (SELECT COUNT(*) AS N
      										FROM TRAGDIP AS D
										      WHERE D.CodTragitto = (SELECT CodTragitto
										                             FROM PRENOTAZIONECARPOOL
										                             WHERE CodPrenotazione = @CodPrenotazione)
										UNION ALL
							      SELECT COUNT(*) AS N
							      FROM TRAGPREM AS P
							      WHERE P.CodTragitto = (SELECT CodTragitto
							                             FROM PRENOTAZIONECARPOOL
							                             WHERE CodPrenotazione =  @CodPrenotazione))AS T);

/*AGGIONRAMENTO CAPIENZA UNA VOLTA TERMINATA LA PRENOTAZIONE*/
UPDATE VEICOLO SET Capienza = (Capienza + @NumUtentiCP + 1) WHERE Targa = TargaVeicoloIn;

/*Inserisci il veicolo della prenotazione da terminare nell'area di sosta con l'indirizzo passato in input*/
INSERT INTO PARCHEGGIO (TargaVeicolo, Indirizzo)
VALUES (TargaVeicoloIn, IndirizzoArrivo);

/*Aggiorna PRENOTAZIONE inserendo orario di termine*/
UPDATE PRENOTAZIONE SET OraFine = current_timestamp() WHERE CodPrenotazione = @CodPrenotazione;

/*Aggiorna l'orario stimato di arrivo nella tappa di arrivo con l'orario effettivo*/
UPDATE TAPPA SET OraStimata = current_timestamp() WHERE Via = IndirizzoArrivo;

/*Calcola la durata della prenotazione utilizzando la funzione timestampdiff*/
SET @durata= (SELECT TIMESTAMPDIFF(SECOND, OraInizio, OraFine)  FROM PRENOTAZIONE  WHERE CodPrenotazione = @CodPrenotazione);

/*Aggiorna la tabella prenotazione aggiungendo la durata*/
UPDATE PRENOTAZIONE SET Durata = (@durata/60)  WHERE CodPrenotazione = @CodPrenotazione;

/*Recuera la tariffa che per semlicità è sempre quella feriale*/
SET @tariffa= (SELECT TariffaFeriale FROM VEICOLO WHERE Targa=TargaVeicoloIn);

/* Calcola il costo totale della prenotazione del veicolo*/
SET @costo=((@tariffa/60)*((@durata/60)));

/*Aggionra il costo della prenotazione*/
UPDATE 	PRENOTAZIONE SET Costo =@costo  WHERE CodPrenotazione = @CodPrenotazione;

END;
COMMIT;
$ DELIMITER ;

/*Controllo prenotazioni in corso*/
DELIMITER $
START TRANSACTION;
CREATE  PROCEDURE checkPrenInCorso (IN EmailUtLoggato VARCHAR(30),  OUT `ind` INT)  BEGIN

        IF((SELECT COUNT(*)
						FROM PRENOTAZIONE as P
						WHERE (EmailUtLoggato= P.EmailUtente) AND (OraFine is null) >0)) THEN
                        SET ind = 1;
				ELSE
                SET ind = 0;
   			END IF;
END;
COMMIT;
$ DELIMITER ;

/*Controllo la tipologia dell'utente per modificare la variabile di sessione 'tipo'*/
DELIMITER $
START TRANSACTION;
CREATE  PROCEDURE checkTipoUtente (IN EmailUtLoggato VARCHAR(30), OUT `ind` INT)
BEGIN

	SET ind = (SELECT COUNT(*) FROM UTPREMIUM WHERE EmailUtP = EmailUtLoggato);

END;
COMMIT;
$ DELIMITER ;

#Procedura di inserimento in TAPPA
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE inserisciTappa (IN IndirizzoIn VARCHAR(50), IN OrdineTappaIn smallint, IN OrarioStimato time)

BEGIN

SET @OrdineTappa = OrdineTappaIn;
#per ora vengono inserite delle coordinate di default, per semplicità

	INSERT INTO TAPPA (Via, Citta, LatTappa, LongTappa, OraStimata) VALUES
    (IndirizzoIn, 'Bologna', '44.528595', '11.292727', OrarioStimato);

END;
COMMIT;
$ DELIMITER ;

/*Procedura per aggiornare il db quando su una prenotazione viene abilitato il carpooling*/
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE NuovaPrenotazioneCarpooling(IN NewCampoNote VARCHAR(200),
											 IN IndirizzoNewPartenza VARCHAR(50),
											 IN IndirizzoNewArrivo VARCHAR(50),
											 IN NewEmail varchar(30),
                                   			 IN TargaNewVeicolo varchar(9))
BEGIN
/*Inserimento nella tabella PRENOTAZIONE*/
INSERT INTO PRENOTAZIONE (CampoNote,OraInizio,OraFine,IndirizzoPartenza,IndirizzoArrivo,EmailUtente,TargaVeicolo)
VALUES (NewCampoNote,current_timestamp(),NULL,IndirizzoNewPartenza,IndirizzoNewArrivo,NewEmail,TargaNewVeicolo);
/*Inserimento nella tabella TRAGITTO*/
INSERT INTO TRAGITTO (Tipologia, KmPercorso) VALUES ('URBANO', FLOOR(RAND()*10)+1);

/*Seleziona id ultima prenotazione inserita*/
SET @CodNewPrenotazione = (SELECT MAX(CodPrenotazione) FROM PRENOTAZIONE);

/*Seleziona id ultimo tragitto inserito*/
SET @CodNewTragitto = (SELECT MAX(CodTragitto) FROM TRAGITTO);

/*Inserimento dei dati sopra ottenuti in PRENOTAZIONECARPOOL*/
INSERT INTO PRENOTAZIONECARPOOL(CodTragitto, CodPrenotazione) VALUES (
	 @CodNewTragitto, @CodNewPrenotazione);
/*Inserimento in CARPOOLPREM/CARPOOLDIP, a seconda del tipo di utente che
 effettua la prenotazione*/
IF ((SELECT COUNT(*) FROM UTPREMIUM WHERE EmailUtP = NewEmail) > 0) THEN
		INSERT INTO CARPOOLPREM(EmailUtente, CodTragitto, TappaPartenza, TappaArrivo) VALUES (
			NewEmail, @CodNewTragitto, IndirizzoNewPartenza, IndirizzoNewArrivo);
	ELSEIF ((SELECT COUNT(*) FROM UTDIPENDENTE WHERE EmailUtD = NewEmail) > 0) THEN
			INSERT INTO CARPOOLDIP(EmailUtente, CodTragitto, TappaPartenza, TappaArrivo) VALUES (
				NewEmail, @CodNewTragitto, IndirizzoNewPartenza, IndirizzoNewArrivo);
	END IF;
/*Aggiornamento iniziale della capienza del veicolo*/
UPDATE VEICOLO SET Capienza = (Capienza - 1) WHERE Targa = TargaNewVeicolo;
END;
COMMIT;
$ DELIMITER ;

#Trigger per inserimento delle tappe in LISTA
DELIMITER $
CREATE TRIGGER InserisciInLista
AFTER INSERT ON TAPPA
FOR EACH ROW

BEGIN

INSERT INTO LISTA(CodTragitto, CodTappa, Ordine) VALUES (
	@CodNewTragitto, NEW.CodTappa, @OrdineTappa);

END;
$ DELIMITER ;

#stampafoto
DELIMITER $
START TRANSACTION;
CREATE  PROCEDURE stampaFoto (IN EmailUtenteIn VARCHAR(30))  BEGIN

	SELECT Immagine
	FROM foto
	WHERE EmailUtente = EmailUtenteIn;
END;
COMMIT;
$ DELIMITER ;

/*Cerca le tappe disponibili per la partenza di un itinerario condiviso per un utente
	dipendente. Dunque cerca solo le tappe di quei tragitti prenotati da utenti che
	lavorano per la stessa azienda dell'utente loggato*/
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE cercaTappeDip(IN EmailUtLoggato VARCHAR(30))
BEGIN

	DROP VIEW IF EXISTS NUMTAPPE;
	/*Seleziona soltanto le tappe di tragitti creati dagli utenti che lavorano per la
		stessa azienda dell'utente loggato*/
	CREATE VIEW NUMTAPPE AS
		SELECT DISTINCT C.EmailUtente, L.CodTragitto, C.TappaArrivo
		FROM LISTA AS L, CARPOOLDIP AS C
		WHERE (L.CodTragitto = C.CodTragitto) AND
					(L.CodTragitto IN (SELECT PC.CodTragitto
														FROM PRENOTAZIONECARPOOL AS PC, PRENOTAZIONE AS P
														WHERE (PC.CodPrenotazione = P.CodPrenotazione) AND
																	(P.OraFine IS NULL)));

		/*Seleziona gli indirizzi delle tappe di partenza da ritornare all'utente*/
		SELECT Via
		FROM TAPPA AS T, LISTA AS L, NUMTAPPE AS NT
		WHERE (T.CodTappa = L.CodTappa) AND
					(L.CodTragitto = NT.CodTragitto) AND
					(Via <> TappaArrivo) AND
					(EmailUtente IN (SELECT EmailUtD
													 FROM UTDIPENDENTE
													 WHERE NomeAzienda = (SELECT NomeAzienda
													 											FROM UTDIPENDENTE
																								WHERE EmailUtD = EmailUtLoggato)));
END;
COMMIT;
$ DELIMITER ;

#Cerca le tappe disponibili per la partenza di un itinerario condiviso
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE cercaTappePrem()
BEGIN

	DROP VIEW IF EXISTS NUMTAPPE;

	/*cerca i codici dei tragitti corrispondenti a prenotazioni in corso (con OraFine NULL) e
	 crea vista contenente il codice di ogni tragitto e il numero di tappe per ogni tragitto*/
	CREATE VIEW NUMTAPPE AS
		SELECT CodTragitto, COUNT(*) AS NumTappe
		FROM LISTA
		WHERE (CodTragitto IN (SELECT CodTragitto
													FROM PRENOTAZIONECARPOOL AS PC, PRENOTAZIONE AS P
													WHERE (PC.CodPrenotazione = P.CodPrenotazione) AND
																(P.OraFine IS NULL)))
		GROUP BY CodTragitto;

		/*Seleziona gli indirizzi delle tappe da ritornare, facendo in modo che le tappe non siano
			le ultime per ordine all'interno del tragitto di cui fanno parte*/
		SELECT Via
		FROM TAPPA AS T, LISTA AS L
		WHERE (T.CodTappa = L.CodTappa) AND
					((L.Ordine + 1) < (SELECT NumTappe
														FROM NUMTAPPE AS NT
														WHERE (NT.CodTragitto = L.CodTragitto)));

END;
COMMIT;
$ DELIMITER ;

#Cerca le prenotazioni con condivisione del tragitto abilitata
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE cercaTratteDisponibili(IN IndirizzoPartenzaIn VARCHAR(50))
BEGIN

	DROP VIEW IF EXISTS TRAGITTILIBERI;

	CREATE VIEW TRAGITTILIBERI AS
		SELECT CodTragitto, IndirizzoPartenza, IndirizzoArrivo, TargaVeicolo,
		 Capienza AS PostiLiberi
		FROM PRENOTAZIONECARPOOL AS PC, PRENOTAZIONE AS P, VEICOLO AS V
		WHERE (PC.CodPrenotazione = P.CodPrenotazione) AND
					(TargaVeicolo = Targa) AND
					(OraFine IS NULL) AND
				  (Capienza > 0);


	SELECT *
	FROM TRAGITTILIBERI
	WHERE CodTragitto IN (SELECT CodTragitto
											FROM LISTA
											WHERE CodTappa = ANY (SELECT CodTappa
	 																			FROM TAPPA
	 																			WHERE Via = IndirizzoPartenzaIn));

END;
COMMIT;
$ DELIMITER ;

DELIMITER $
START TRANSACTION;
CREATE PROCEDURE cercaTratteDisponibiliDip(IN IndirizzoPartenzaIn VARCHAR(50),
																					 IN EmailUtLoggato VARCHAR(30))
BEGIN

	DROP VIEW IF EXISTS TRAGITTILIBERI;

	CREATE VIEW TRAGITTILIBERI AS
		SELECT CodTragitto, EmailUtente, IndirizzoPartenza, IndirizzoArrivo, TargaVeicolo,
		 Capienza AS PostiLiberi
		FROM PRENOTAZIONECARPOOL AS PC, PRENOTAZIONE AS P, VEICOLO AS V
		WHERE (PC.CodPrenotazione = P.CodPrenotazione) AND
					(TargaVeicolo = Targa) AND
				  (Capienza > 0);

	SET @AziendaUt = (SELECT NomeAzienda
										FROM UTDIPENDENTE
										WHERE EmailUtD = EmailUtLoggato);

	SELECT CodTragitto, IndirizzoPartenza, IndirizzoArrivo, TargaVeicolo, PostiLiberi
	FROM TRAGITTILIBERI
	WHERE CodTragitto IN (SELECT CodTragitto
												FROM LISTA
												WHERE CodTappa = ANY (SELECT CodTappa
	 										 												FROM TAPPA
	 										 												WHERE Via = IndirizzoPartenzaIn))
   AND @AziendaUt = (SELECT NomeAzienda
	 									  FROM UTDIPENDENTE
										  WHERE EmailUtD = EmailUtente);

END;
COMMIT;
$ DELIMITER ;

#Seleziona le tappe del tragitto dato in input, meno quelle precedenti alla
#tappa data in input
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE SelezionaTappeArrivo(IN TragittoIn smallint, IN Partenza VARCHAR(50))
BEGIN
/*dato che potrebbero esserci più tappe con la stessa via, uso l'operatore MAX per recuperare l'id
	della tappa inserita per ultima (quindi quella con id maggiore) nel caso vi siano omonimie nella Via*/
	SET @CodTappaPartenza = (SELECT MAX(CodTappa)
													 FROM TAPPA
													 WHERE Via = Partenza);

	SELECT Via, CodTragitto, oraStimata
	FROM TAPPA AS T, LISTA AS L
	WHERE (T.CodTappa = L.CodTappa) AND
				(L.CodTragitto = TragittoIn) AND
				(T.CodTappa > @CodTappaPartenza);

END;
COMMIT;
$ DELIMITER ;

#Inserisce la prenotazione di un tragitto condiviso da parte di un utente dipendente
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE prenotaTragittoDip(IN CodTragittoIn smallint,
									IN emailUT VARCHAR(30),
									IN IndirizzoPartenzaIn VARCHAR(50),
									IN IndirizzoArrivoIn VARCHAR(50))
BEGIN

/*dato che potrebbero esserci più tappe con la stessa via, uso l'operatore MAX per recuperare l'id
	della tappa inserita per ultima (quindi quella con id maggiore) nel caso vi siano omonimie nella Via*/
SET @IdPartenza = (SELECT MAX(CodTAppa)
									 FROM TAPPA
								 	 WHERE Via = IndirizzoPartenzaIn);

SET @IdArrivo = (SELECT MAX(CodTAppa)
								 FROM TAPPA
								 WHERE Via = IndirizzoArrivoIn);

INSERT INTO TRAGDIP(EmailUtente, CodTragitto, TappaPartenza, TappaArrivo)
VALUES (emailUT, CodTragittoIn, @IdPartenza, @IdArrivo);

END;
COMMIT;
$ DELIMITER ;

#Trigger che aggiorna la capienza del veicolo dopo inserimento in TRAGDIP
DELIMITER $
CREATE TRIGGER AggiornaCapienzaDip
AFTER INSERT ON TRAGDIP
FOR EACH ROW
BEGIN

SET @TargaVeicolo = (SELECT TargaVeicolo
					 FROM PRENOTAZIONE AS P JOIN PRENOTAZIONECARPOOL AS PC
					 WHERE (P.CodPrenotazione = PC.CodPrenotazione) AND
									 				 (PC.CodTragitto = NEW.CodTragitto));

UPDATE VEICOLO SET Capienza = (Capienza - 1) WHERE Targa = @TargaVeicolo;

END;
$ DELIMITER ;

#Inserisce la prenotazione di un tragitto condiviso da parte di un utente premium
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE prenotaTragittoPrem(IN CodTragittoIn smallint,
									 IN emailUT VARCHAR(30),
									 IN IndirizzoPartenzaIn VARCHAR(50),
									 IN IndirizzoArrivoIn VARCHAR(50))
BEGIN

SET @IdPartenza = (SELECT CodTAppa
									 FROM TAPPA
								 	 WHERE Via = IndirizzoPartenzaIn);

SET @IdArrivo = (SELECT CodTAppa
								 FROM TAPPA
								 WHERE Via = IndirizzoArrivoIn);

INSERT INTO TRAGPREM(EmailUtente, CodTragitto, TappaPartenza, TappaArrivo)
VALUES (emailUT, CodTragittoIn, @IdPartenza, @IdArrivo);

END;
COMMIT;
$ DELIMITER ;

#Trigger che aggiorna la capienza del veicolo dopo inserimento in TRAGPREM
DELIMITER $
CREATE TRIGGER AggiornaCapienzaPrem
AFTER INSERT ON TRAGPREM
FOR EACH ROW
BEGIN

SET @TargaVeicolo = (SELECT TargaVeicolo
										 FROM PRENOTAZIONE AS P JOIN PRENOTAZIONECARPOOL AS PC
									 	 WHERE (P.CodPrenotazione = PC.CodPrenotazione) AND
									 				 (PC.CodTragitto = NEW.CodTragitto));

UPDATE VEICOLO SET Capienza = (Capienza - 1) WHERE Targa = @TargaVeicolo;

END;
$ DELIMITER ;
#Procedura che recupera le infromazioni dei tragitti condivisi da un utente premium
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE VisualizzaTragittiUtentePrem(IN EmailUtLoggato VARCHAR(30))
BEGIN

SELECT CodTragitto, TappaPartenza, TappaArrivo FROM CARPOOLPREM
WHERE EmailUtente = EmailUtLoggato
UNION
SELECT CodTragitto, T1.Via, T2.Via
FROM TRAGPREM, TAPPA AS T1, TAPPA AS T2
WHERE (TappaPartenza = T1.CodTappa) AND
			(TappaArrivo = T2.CodTappa) AND
			(EmailUtente = EmailUtLoggato);

END;
COMMIT;
$ DELIMITER ;

#Procedura che recupera le infromazioni dei tragitti condivisi da un utente dipendente
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE VisualizzaTragittiUtenteDip(IN EmailUtLoggato VARCHAR(30))
BEGIN

SELECT CodTragitto, TappaPartenza, TappaArrivo FROM CARPOOLDIP
WHERE EmailUtente = EmailUtLoggato
UNION
SELECT CodTragitto, T1.Via, T2.Via
FROM TRAGDIP, TAPPA AS T1, TAPPA AS T2
WHERE (TappaPartenza = T1.CodTappa) AND
			(TappaArrivo = T2.CodTappa) AND
			(EmailUtente = EmailUtLoggato);

END;
COMMIT;
$ DELIMITER ;

#procedura di ricerca utenti dipendenti che partecipano al tragitto
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE cercaUtentiTragittoDip(IN CodTragIn smallint,
										IN EmailUtLoggato varchar(30))
BEGIN

SELECT EmailUtente, CodTragitto
FROM TRAGDIP
WHERE (CodTragitto = CodTragIn) AND
			(EmailUtente != EmailUtLoggato)
UNION
SELECT EmailUtente, CodTragitto
FROM CARPOOLDIP
WHERE (CodTragitto = CodTragIn) AND
			(EmailUtente != EmailUtLoggato);

END;
COMMIT;
$ DELIMITER ;

#procedura di ricerca utenti premium che partecipano al tragitto

DELIMITER $
START TRANSACTION;
CREATE PROCEDURE cercaUtentiTragittoPrem(IN CodTragIn smallint,
										 IN EmailUtLoggato varchar(30))
BEGIN

SELECT EmailUtente, CodTragitto
FROM TRAGPREM
WHERE (CodTragitto = CodTragIn) AND
			(EmailUtente != EmailUtLoggato)
UNION
SELECT EmailUtente, CodTragitto
FROM CARPOOLPREM
WHERE (CodTragitto = CodTragIn) AND
			(EmailUtente != EmailUtLoggato);

END;
COMMIT;
$ DELIMITER ;

#procedura inserimento valutazione
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE inserisciValutazione(IN ValutatoreIn VARCHAR(30),
									  IN ValutatoIn VARCHAR(30),
									  IN CodTragIn SMALLINT,
									  IN TestoIn VARCHAR(500),
									  IN DataTestoIn DATE,
									  IN VotoIn SMALLINT)
BEGIN

INSERT INTO VALUTAZIONE(UtenteValutatore, UtenteValutato, CodTragitto,
												Testo, DataTesto, Voto)
					VALUES (ValutatoreIn, ValutatoIn, CodTragIn, TestoIn, DataTestoIn,
									VotoIn);

END;
COMMIT;
$ DELIMITER ;

#INSERIMENTO SEGNALAZIONE
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE inserisciSegnalazione(IN ValutatoreIn VARCHAR(30),
									   IN CodTargaIn varchar(9),
									   IN TitoloIn varchar(30),
									   IN TestoIn VARCHAR(500),
									   IN DataTestoIn DATE)
BEGIN

SET @EmailUtSegnalatore = ValutatoreIn;

INSERT INTO FEEDBACK(Titolo,Testo,DataInserimento,TargaVeicolo)
					VALUES (TitoloIn, TestoIn, DataTestoIn, CodTargaIn);

END;
COMMIT;
$ DELIMITER ;

#Trigger che aggiorna la tabella feedbut dopo che utente invia segnalazione
DELIMITER $
CREATE TRIGGER AggiornaFeedUtente
AFTER INSERT ON FEEDBACK
FOR EACH ROW

BEGIN

	INSERT INTO FEEDUTENTE (EmailUt) VALUES (@EmailUtSegnalatore);

END;
$ DELIMITER ;

#procedura di inserimento tariffa per veicoli prenotati
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE CalcolaTariffa(IN TargaVeicolo VARCHAR(30), IN NuovaTariffa smallint)
BEGIN

		IF EXISTS((SELECT Targa FROM VEICOLO WHERE(VEICOLO.Targa = TargaVeicolo))) THEN
        INSERT INTO VEICOLO(Tariffa)
		VALUES (NuovaTariffa);

	END IF;
END;
COMMIT;
$ DELIMITER ;

#procedura query coordinate
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE Coordinate ()
BEGIN
	 select Latitudine, longitudine from PARCHEGGIO, AREASOSTA where PARCHEGGIO.indirizzo= AREASOSTA.indirizzo;
END;
commit;
$ DELIMITER ;

#Procedura per statistiche voti

DELIMITER $
START TRANSACTION;
CREATE PROCEDURE classificaUtenti()
BEGIN

		Select UtenteValutato, AVG(Voto) AS MediaVoto
		FROM VALUTAZIONE
		GROUP BY UtenteValutato
		ORDER BY  MediaVoto DESC;

END;
commit;
$ DELIMITER ;

#Procedura per classifica segnalazioni
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE classificaSegnalazioni()
BEGIN

		SELECT EmailUt, COUNT(*) as totSegnalazioni
    FROM FEEDUTENTE
    GROUP BY EmailUt
    ORDER BY totSegnalazioni DESC;

END;
commit;
$ DELIMITER ;

#procedura per classifica dei veicoli piu prenotati

DELIMITER $
START TRANSACTION;
CREATE PROCEDURE classificaVeicoli()
BEGIN
		Select Modello, count(*) as totPrenotazioni
        from prenotazione, veicolo
        WHERE prenotazione.TargaVeicolo=veicolo.Targa
		GROUP BY Modello ORDER BY  totPrenotazioni desc;

END;
commit;
$ DELIMITER ;

#procedura utile per la stampa delle valutazioni
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE ValutazioniUtente (IN UtenteIn VARCHAR(50))  BEGIN

		SELECT UtenteValutatore, DataTesto , Voto, Testo
    FROM VALUTAZIONE
    WHERE UtenteValutato = UtenteIn;

END;
commit;
$ DELIMITER ;

#procedura recuperare la brochure
DELIMITER $
START TRANSACTION;
CREATE PROCEDURE brochure (IN NomeSocIn VARCHAR(50))  BEGIN

	SELECT NomeBrochure from BROCHURE WHERE NomeSocieta = NomeSocIn;
END;
commit;
$ DELIMITER ;
