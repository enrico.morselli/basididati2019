<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tratte disponibili</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!--inserimento header-->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <table class="table table-dark" style="background: rgba(0,0,0,0.5)">
        <thead class="thead-dark">
          <tr align="center">
            <!--header della tabella-->
            <th colspan="6" scope="col"><h3 class="text-center text-justify">Tragitti disponibili</h3></th>
          </tr>
          <tr>
            <!--header di ciascuna colonna-->
            <th scope="col">#</th>
            <th scope="col">Partenza da</th>
            <th scope="col">Arrivo a</th>
            <th scope="col">Targa veicolo</th>
            <th scope="col">Posti liberi</th>
            <th scope="col">Prenota tragitto</th>
          </tr>
        </thead>
        <?php if (isset($resp)): ?>
          <tbody>
            <?php foreach($resp as $trag): ?>
            <tr>
              <form action="selezionaArrivo.php" method="post">
                <th scope="row" style="width: 5%">
                  <?php echo ($trag['CodTragitto']); ?></th>
                <td style="width: 10%"><?php echo ($trag['IndirizzoPartenza']); ?></td>
                <td style="width: 10%"><?php echo ($trag['IndirizzoArrivo']); ?></td>
                <td style="width: 10%"><?php echo ($trag['TargaVeicolo']); ?></td>
                <td style="width: 10%"><?php echo ($trag['PostiLiberi']); ?></td>
                <td style="width: 5%">
                  <input type="hidden" name="CodTragitto" value="<?php echo ($trag['CodTragitto']); ?>">
                  <input type="hidden" name="IndirizzoPartenza" value="<?php echo ($IndirizzoPartenza); ?>">
                  <input type="submit" class="btn btn-outline-light" name="action" value="PRENOTA">
                </td>
              </form>
            </tr>
          <?php endforeach;
        else: ?>
        <tr align="center">
            <td colspan="5"><strong>NON CI SONO TRAGITTI DISPONIBILI</strong></td>
        </tr>
      <?php endif; ?>
          </tbody>
        </table>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <?php include "footer.php"; ?>

  </body>
</html>
