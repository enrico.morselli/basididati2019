<html>
  <head>
    <title>Registrazione Utente</title>
    <script>
           function show() {
              var selectBox = document.getElementById('tipoUtente');
              var userInput = selectBox.options[selectBox.selectedIndex].value;
              if (userInput === 'UD') {
                 document.getElementById('datiAttività').style.visibility = "visible";
              } else {
                 document.getElementById('datiAttività').style.visibility = "hidden";
              }
           }
        </script>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
          maximum-scale=1.0, minimum-scale=1.0">
          <!-- fogli di stile -->
          <link rel="stylesheet" href="css/bootstrap.min.css">
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
          <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>

  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!-- inseriemnto menu -->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <form method='post' action = "registrazioneUtente.php" id='dati'>
      <div id="divR" align="center">
      <table class="table table-dark" style="background: rgba(0,0,0,0.5); width: 50%">
        <thead class="thead-dark">
          <tr align="center">
            <th colspan="2" scope="col"><h3 class="text-center text-justify">Registrazione Utente</h3></th>
          </tr>
        </thead>
          <tr> <!--l'attributo required richiede che il campo sia riempito prima di inviare il form-->
              <td><h5 class="text-left font-weight-light">Email:</h5></td>
              <!--tipo di campo email verifica che il testo inserito sia un'email-->
              <td><input class="form-control mr-sm-2" type="email" maxlength="50" name="Email" placeholder="Email..." required></td>
          </tr>
          <tr>
              <td><h5 class="text-left font-weight-light">Nome:</h5></td>
              <td><input class="form-control mr-sm-2" type="long" maxlength="200" name="Nome" placeholder="Nome..."required></td>
          </tr>

          <tr>
              <td><h5 class="text-left font-weight-light">Cognome:</h5></td>
              <td><input class="form-control mr-sm-2" type="text" maxlength="20" name="Cognome" placeholder="Cognome..."required></td>
          </tr>
          <tr>
              <td><h5 class="text-left font-weight-light">Password:</h5></td>
              <td><input class="form-control mr-sm-2"  type="password" maxlength="20" name="PasswordEm" placeholder="Password..." required></td>
          </tr>
          <tr>
              <td><h5 class="text-left font-weight-light">Ripeti Password:</h5></td>
              <td><input class="form-control mr-sm-2" type="password" maxlength="20" name="Password2" placeholder="Ripeti password..." required></td>
          </tr>
          <tr>
              <td><h5 class="text-left font-weight-light">Data nascita:</h5></td>
              <td><input class="form-control mr-sm-2" type="date" name="DataNascita" required></td>
          </tr>
          <tr>
              <td><h5 class="text-left font-weight-light">Luogo nascita: </h5></td>
              <td><input class="form-control mr-sm-2" type="long" maxlength="200" name="LuogoNascita" placeholder="Luogo nascita..." required></td>
          </tr>
          <tr>
            <td><h5 class="text-left font-weight-light">Tipo utente: </h5></td>
            <td>
              <!-- Select box per selezionare il tipo di utente -->
              <select class="form-control mr-sm-2" id ="tipoUtente" name ="tipoUtente" onchange="return show()" required>
                <option disabled selected value="">Seleziona tipo utente</option>
                <option value="US">Utente Semplice</option>
                <option value="UD">Utente Dipendente</option>
              </select>
            </td>
          </tr>
          <tr style="visibility: hidden" id="datiAttività">
            <td><h5 class="text-left font-weight-light">Nome Azienda: </h5></td>
            <td>
              <select class="form-control" name="NomeAzienda" form="dati" >
                <option disabled selected value="">Seleziona Azienda</option>
                <?php
                  require 'db.inc.php';
                  try {
                    //query per recuperare gli indirizzi delle aree di sosta
                    $sql = 'SELECT Nome
                            FROM AZIENDA';
                    $stmt = $pdo -> prepare($sql);
                    $stmt -> execute();
                  } catch (Exception $e) {
                    echo "Errore nella query: ".$e;
                    exit();
                  }
                  //recupero il risultato della query
                  $res = $stmt -> fetchAll(PDO::FETCH_ASSOC);
                  //scorro il risultato
                  foreach ($res as $row): ?>
                  <option value="<?php echo $row['Nome']; ?>">
                    <p><?php echo $row['Nome']; ?></p>
                  </option>
                <?php endforeach; ?>
              </select>
            </td>
          </tr>
      </table>
      <input type ="submit" class="btn btn-lg btn-outline-light" style="background: rgba(0,0,0,0.5)" name="action" value="REGISTRA UTENTE" class="btn btn-lg btn-primary">
      </div>
    </form>
  </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <?php include "footer.php"; ?>

  </body>

  </html>
