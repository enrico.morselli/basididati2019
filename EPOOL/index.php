<?php
require 'db.inc.php';
//connessione al database mongodb
require 'mongodb.inc.php';
//funzione per creare finestra di alert javascript
function alert($msg) {
   echo "<script type='text/javascript'>alert('$msg');</script>";
}

session_start();


//se viene richiesto form di carsharing
if (isset($_POST['action']) and $_POST['action'] == 'CARSHARING') {
  //reindirizzamento al form di carsharing
  if (isset($_SESSION['email'])) {
    //controlla se l'utente loggato ha delle prenotazioni in corso
    //in quel caso, mostra l'alert. In caso contrario, mostra il form
    $email = $_SESSION['email'];
    $query = 'CALL checkPrenInCorso(?,@out)';
    $stmt = $pdo->prepare($query);
    $stmt->bindParam(1, $email );
    $stmt->execute();
    $output = $pdo->query("select @out;")->fetch();
    $numPren = $output['@out'];


    if($numPren == 1){
      echo "<script>alert('TERMINA PRENOTAZIONE PRECEDENTE'); window.location = './storicoPrenotazioni.php';</script>";
    } else {
      include 'cordinate.php';
    header("Location: cercaAutoDisponibili.php");
  }

  } else {
    alert("ACCEDI O REGISTRATI");
  }

//se viene richiesto il form di carpooling
} elseif (isset($_POST['action']) and $_POST['action'] == 'CARPOOLING') {
  //INSERIRE reindirizzamento al form di carpooling
  if (isset($_SESSION['email'])) {
    //controlla se l'utente è di tipo US, in quel caso non può accedere al
    //servizio di carpooling e resta nella pagina principale
    if ($_SESSION['tipo'] == "US") {
      alert("Non puoi accedere al servizio");
    } else { //in caso contrario, indirizza al form di carpooling
      //controlla se l'utente loggato ha delle prenotazioni in corso
      //in quel caso, mostra l'alert. In caso contrario, mostra il form
      $email = $_SESSION['email'];
      $query = 'CALL checkPrenInCorso(?,@out)';
      $stmt = $pdo->prepare($query);
      $stmt->bindParam(1, $email );
      $stmt->execute();
      $output = $pdo->query("select @out;")->fetch();
      $numPren = $output['@out'];


      if($numPren == 1){
        echo "<script>alert('TERMINA PRENOTAZIONE PRECEDENTE'); window.location = './storicoPrenotazioni.php';</script>";
      } else {
        include 'cordinate.php';

      header("Location: formCarpooling.html.php");
    }

}
}
}
if (isset($_SESSION['email'])) {
$data=date("Y-m-d H:i:s");
$bulkWrite = new MongoDB\Driver\BulkWrite;
$doc = ['avviso'=> 'effettuato accesso',
'utente' => $_SESSION['email'], 'data' => $data];
$bulkWrite->insert($doc);
$manager->executeBulkWrite('epool.logEpool', $bulkWrite);
}
include "formIndex.html.php";
