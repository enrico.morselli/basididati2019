<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Auto Più Usate</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!--inserimento header-->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <table class="table table-dark" align="center" style="background: rgba(0,0,0,0.5); width: 50%">
        <thead class="thead-dark">
          <tr align="center">
            <!--header della tabella-->
            <th colspan="5" scope="col"><h3 class="text-center text-justify">Auto più usate</h3></th>
          </tr>
          <tr>
            <!--header di ciascuna colonna-->
            <th scope="col">#</th>
            <th scope="col">Modello</th>
            <th scope="col">Totale prenotazioni:</th>
          </tr>
        </thead>
        <?php
        $num=0;
        if (isset($resp)): ?>
          <tbody>
            <?php //scorre il risultato della query e riempie le celle della tabella
            foreach ($resp as $listaAuto): ?>
            <tr>
              <th scope="row" style="width: 5%"><?php echo $num + 1;
              $num = $num + 1; ?></th>
              <td style="width: 10%"><?php echo $listaAuto['Modello']; ?></td>
              <td style="width: 10%"><?php echo $listaAuto['totPrenotazioni']; ?></td>
            </tr>
        <?php endforeach;
        else: ?>
            <tr align="center">
              <td colspan="5"><strong>ANCORA NESSUNA CLASSIFICA</strong></td>
            </tr>
        <?php endif; ?>
          </tbody>
        </table>
      </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <?php include "footer.php"; ?>

  </body>
</html>
