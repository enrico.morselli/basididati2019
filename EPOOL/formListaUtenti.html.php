<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Lista Utenti</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!-- header -->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <table class="table table-dark" style="background: rgba(0,0,0,0.5);">
        <thead class="thead-dark">
          <tr align="center">
            <!--header della tabella-->
            <th colspan="7" scope="col"><h3 class="text-center text-justify text-light">Utenti Registrati</h3></th>
          </tr>
          <tr>
            <!--header di ciascuna colonna-->
            <th scope="col">#</th>
            <th scope="col">Email</th>
            <th scope="col">Foto</th>
            <th scope="col">Data nascita</th>
            <th scope="col">Luogo Nascita</th>
            <th scope="col">Nome</th>
            <th scope="col">Cognome</th>
          </tr>
        </thead>
        <?php
        //variabile per la colonna #
        $numeroUtenti = 0;
        if (isset($resp)): ?>
          <tbody>
            <?php //scorre il risultato della query e riempie le celle della tabella
            foreach ($resp as $utente): ?>

            <form method='post' action = "fotoUtente.html.php" id='dati'>
            <tr>
              <th scope="row" style="width: 5%"><?php echo $numeroUtenti + 1;
              $numeroUtenti = $numeroUtenti + 1; ?></th>
              <td style="width: 10%"> <?php echo $utente['Email']; ?></td>
              <td style="width: 10%"><input  type="hidden" value="<?php echo $utente['Email']; ?>" name="email"> <input type="submit" name="action"  value="FOTO" class="btn btn-outline-light"></td>
              <td style="width: 10%"><?php echo $utente['dataNascita']; ?></td>
              <td style="width: 10%"><?php echo $utente['luogoNascita']; ?></td>
              <td style="width: 10%"><?php echo $utente['Nome']; ?></td>
              <td style="width: 10%"><?php echo $utente['Cognome']; ?></td>
            </tr>
          </form>
        <?php endforeach;
        else: ?>
            <tr align="center">
              <td colspan="5"><strong>NON CI SONO UTENTI NEL DATABASE</strong></td>
            </tr>
        <?php endif; ?>
          </tbody>
        </table>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <?php include "footer.php"; ?>

  </body>
</html>
