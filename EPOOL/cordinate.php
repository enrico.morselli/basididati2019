<?php
//connessione al database mongodb
require 'mongodb.inc.php';

function get_data() {

  require 'db.inc.php';

  try {
    $query = 'CALL Coordinate()';
    $stmt = $pdo -> prepare($query);
    if (!$stmt -> execute()) {
      echo "Errore nella query: " . $dbc -> error. ".";
    } else {
      $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
    }
  } catch (Exception $e) {
    echo "Errore: " . $e . ".";
    exit();
  }
  $cordinate = array();
  foreach ($resp as $row):

    $cordinate[]=array(
      'latitude'    => $row["Latitudine"],
      'longitude'   =>  $row["longitudine"]
    );
    endforeach;
    //Returns a string containing the JSON representation of the supplied value.
    return json_encode($cordinate);
}
$nome="cords";
$file_name=$nome . '.json';

//scrive le coordinate dei parcheggi, ottenute con get_data() nel file 'cords.json'
if(file_put_contents($file_name, get_data()))
{
  //echo $file_name . ' file created';
}else{
//  echo "errore";
}
?>
