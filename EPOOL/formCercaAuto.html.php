<!DOCTYPE html>
<html>
  <head>
    <title>Auto Disponibili</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.0/mapsjs-ui.css?dp-version=1549984893" />
    <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-core.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-service.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-clustering.js"></script>
    <script  type="text/javascript"src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!-- inseriemnto header -->
    <?php include 'header.php'; ?>
        <div class="container" id="divContainer">
          <!--tabella con le auto disponibili per la prenotazione-->
            <table class="table table-dark" style="background: rgba(0,0,0,0.5)">
                <thead class="thead-dark">
                  <tr align="center">
                      <!--header tabella-->
                      <th colspan="6" scope="col"><h3 class="text-center text-justify">AUTO DISPONIBILI</h3></th>
                  </tr>
                  <tr>
                    <!--header delle colonne-->
                    <th scope="col">#</th>
                    <th scope="col">Modello</th>
                    <th scope="col">Indirizzo</th>
                    <th scope="col">Targa</th>
                    <th scope="col">Tariffa Oraria</th>
                    <th scope="col">Prenota</th>
                  </tr>
              </thead>
              <?php $numeroAuto = 0;
              //se $resp è inizializzata, scorri il contenuto
              if (isset($resp)): ?>
                 <tbody>
                   <?php foreach ($resp as $auto): ?>
                        <tr>
                            <form action="inizializzaPrenotazione.php" method="post">
                            <th scope="row" style="width: 5%">
                              <?php echo($numeroAuto + 1);
                              $numeroAuto = $numeroAuto + 1; ?></th>
                            <!--cella contenente il modello dell'auto-->
                            <td style="width: 10%">
                              <?php echo($auto['Modello']); ?></td>
                            <!--cella contenente l'indirizzo dell'area di sosta-->
                            <td style="width: 10%">
                              <?php echo($auto['Indirizzo']); ?></td>
                            <!--cella contenente la targa del veicolo-->
                            <td style="width: 10%">
                              <?php echo($auto['TargaVeicolo']); ?></td>
                              <!--cella contenente la tariffa oraria del veicolo-->
                              <td style="width: 10%">
                                <?php echo($auto['TariffaFeriale']); ?></td>
                            <!--bottone per effettuare la prenotazione del veicolo-->
                            <td style="width: 3%">
                            <input type="hidden" name="TargaVeicolo"
                              value="<?php echo $auto['TargaVeicolo'];?>">
                            <input type="submit" class="btn btn-outline-light my-2 my-sm-0"
                            name="action" value="PRENOTA VEICOLO"></td>
                          </form>
                        </tr>
                      <?php endforeach;
                      //se $resp non è inizializzata
                      else: ?>
                     <tr align="center">
                         <td colspan="5"><strong>NON CI SONO AUTO DISPONIBILI</strong></td>
                     </tr>
                    <?php endif; ?>
              </tbody>
          </table>
        </div>
        <!-- mappa con i marker raffiguranti i veicoli disponibili -->
        <div id="map" style="width: 100%; height: 400px; background: grey" >
        <script  type="text/javascript" charset="UTF-8" >
          /**
       * Display clustered markers on a map
       *
       * Note that the maps clustering module http://js.api.here.com/v3/3.0/mapsjs-clustering.js
       * must be loaded to use the Clustering

       * @param {H.Map} map A HERE Map instance within the application
       * @param {Array.<Object>} data Raw data that contains airports' coordinates
       */
      function startClustering(map, data) {
        // First we need to create an array of DataPoint objects,
        // for the ClusterProvider
        var dataPoints = data.map(function (item) {
          return new H.clustering.DataPoint(item.latitude, item.longitude);
        });

        // Create a clustering provider with custom options for clusterizing the input
        var clusteredDataProvider = new H.clustering.Provider(dataPoints, {
          clusteringOptions: {
            // Maximum radius of the neighbourhood
            eps: 32,
            // minimum weight of points required to form a cluster
            minWeight: 10
          }
        });

        // Create a layer tha will consume objects from our clustering provider
        var clusteringLayer = new H.map.layer.ObjectLayer(clusteredDataProvider);

        // To make objects from clustering provder visible,
        // we need to add our layer to the map
        map.addLayer(clusteringLayer);
      }


      /**
       * Boilerplate map initialization code starts below:
       */

      // Step 1: initialize communication with the platform
      var platform = new H.service.Platform({
        app_id: 'devportal-demo-20180625',
        app_code: '9v2BkviRwi9Ot26kp2IysQ',
        useHTTPS: true
      });
      var pixelRatio = window.devicePixelRatio || 1;
      var defaultLayers = platform.createDefaultLayers({
        tileSize: pixelRatio === 1 ? 256 : 512,
        ppi: pixelRatio === 1 ? undefined : 320
      });

      // Step 2: initialize a map
      var map = new H.Map(document.getElementById('map'), defaultLayers.normal.map, {
        center: new H.geo.Point(44.4936714, 11.3430347),
        zoom: 14,
        pixelRatio: pixelRatio
      });

      // Step 3: make the map interactive
      // MapEvents enables the event system
      // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
      var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));


      // Step 4: create the default UI component, for displaying bubbles
      var ui = H.ui.UI.createDefault(map, defaultLayers);

      // Step 5: request a data about airports's coordinates
      jQuery.getJSON('cords.json', function (data) {
        startClustering(map, data);
      });

        </script>
      </div>
      <?php include "footer.php"; ?>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


  </body>
</html>
