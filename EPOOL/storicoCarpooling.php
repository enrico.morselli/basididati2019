<?php
//connessione al database mongodb
require 'mongodb.inc.php';
//script per la visualizzazione della lista degli utenti registrati
require 'db.inc.php';
//funzione per creare finestra di alert javascript
function alert($msg) {
   echo "<script type='text/javascript'>alert('$msg');</script>";
}

session_start();
$utente = $_SESSION['email'];
$tipo = $_SESSION['tipo'];

try {

  if ($tipo == "UP") {
    $query = 'CALL VisualizzaTragittiUtentePrem(?)';
  } elseif ($tipo == "UD") {
    $query = 'CALL VisualizzaTragittiUtenteDip(?)';
  }

  $stmt = $pdo -> prepare($query);
  $stmt -> bindParam(1, $utente);
  if (!$stmt -> execute()) {
    echo "Errore nella query " . $dbc -> error. ".";
  } else {
    $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
  }
} catch (Exception $e) {
  echo "Errore: " . $e . ".";
  exit();
}

include 'formStoricoCarpooling.html.php';
if (isset($_SESSION['email'])) {
$data=date("Y-m-d H:i:s");
$bulkWrite = new MongoDB\Driver\BulkWrite;
$doc = ['avviso' => 'storico prenotazioni carpooling',
  'utente' => $_SESSION['email'], 'data' => $data ];
$bulkWrite->insert($doc);
$manager->executeBulkWrite('epool.logEpool', $bulkWrite);
}
