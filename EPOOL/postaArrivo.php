<?php
//connessione al database mongodb
require 'mongodb.inc.php';

   function alert($msg) {
      echo "<script type='text/javascript'>alert('$msg');</script>";
   }
   // Connessione al DB
   require 'db.inc.php';
   //richiamo la stored procedure per visualizzare la lista dei messaggi privati
   try{
      session_start();
      $utente = $_SESSION['email'];
      $query = "CALL listaMessaggi(?)";
      $stmt = $pdo->prepare($query);
      $stmt->bindParam(1, $utente);
      if(!$stmt->execute()){
         echo "Errore della query: " .$dbc->error . ".";
      }else{
         $resp = $stmt->fetchAll();
      }
   }catch(Exception $ex){
      alert("errore procedura");
      exit();
   }
   //faccio un ciclo per ogni riga risultante dalla chiamata della procedura
   foreach ($resp as $row) {
      //controllo se il NickMittente è uguale all'utente che ha effettuato il login, nel caso lo sia allora assegno la riga
      //all'array dei messaggi inviati, altrimenti la assegno a quello dei messaggi ricevuti
      if($row['emailMittente'] == $_SESSION['email']){
         $messaggiInviati[] = array('Destinatario' =>$row['emailDestinatario'],
                                    'titolo' => $row['Titolo'], 'testo' => $row['Testo'], 'data' => $row["Data"]);
      }else{
         $messaggiRicevuti[] = array('Mittente' =>$row['emailMittente'], 'titolo' => $row['Titolo'],
                                     'testo' => $row['Testo'], 'data' => $row["Data"]);
      }
   }

include "formPostaArrivo.html.php";
