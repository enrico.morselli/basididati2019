
<!DOCTYPE html>
<html>
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
        maximum-scale=1.0, minimum-scale=1.0">
        <!-- fogli di stile -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
      <!-- inseriemnto menu -->
      <?php include 'header.php'; ?>
      <div id="divContainer">
        <div class="container">
          <table class="table table-dark" style="background: rgba(0,0,0,0.5); ">
            <thead class="thead-dark">
              <tr align="center">
                <th colspan="5" scope="col">MESSAGGI RICEVUTI</th>
              </tr>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Mittente</th>
                <th scope="col">Oggetto</th>
                <th scope="col" style="text-align: center">Testo</th>
                <th scope="col">Data</th>
              </tr>
            </thead>
            <?php $numeroMessR = 0;
              if (isset($messaggiRicevuti)): ?>
                   <tbody>
            <?php foreach ($messaggiRicevuti as $messaggioR): ?>
                      <tr>
                        <th scope="row" style="width: 5%"><?php echo($numeroMessR + 1);
                            $numeroMessR = $numeroMessR + 1; ?></th>
                          <td style="width: 10%"><?php echo($messaggioR['Mittente']); ?></td>
                          <td style="width: 10%"><?php echo($messaggioR['titolo']); ?></td>
                          <td style="width: 65%"><?php echo($messaggioR['testo']); ?></td>
                          <td style="width: 10%"><?php echo($messaggioR['data']); ?></td>
                      </tr>
              <?php endforeach;
              else: ?>
                       <tr align="center">
                           <td colspan="5"><strong>NON HAI RICEVUTO NESSUN MESSAGGIO</strong></td>
                       </tr>
              <?php endif; ?>
                </tbody>
            </table>
        </div>
        <div class="container">
          <table class="table table-dark" style="background: rgba(0,0,0,0.5); ">
            <thead class="thead-dark">
              <tr align="center">
                <th colspan="7" scope="col">MESSAGGI INVIATI</th>
              </tr>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Destinatario</th>
                <th scope="col">Oggetto</th>
                <th scope="col" style="text-align: center">Testo</th>
                <th scope="col">Data</th>
              </tr>
          </thead>
          <?php $numeroMessI = 0;
          if (isset($messaggiInviati)): ?>
          <tbody>
          <?php foreach ($messaggiInviati as $messaggioI): ?>
            <tr>
             <th scope="row" style="width: 5%"><?php echo($numeroMessI + 1);
              $numeroMessI = $numeroMessI + 1; ?></th>
              <td style="width: 10%"><?php echo($messaggioI['Destinatario']); ?></td>
              <td style="width: 10%"><?php echo($messaggioI['titolo']); ?></td>
              <td style="width: 65%"><?php echo($messaggioI['testo']); ?></td>
              <td style="width: 10%"><?php echo($messaggioI['data']); ?></td>
            </tr>
          <?php endforeach;
          else: ?>
            <tr align="center">
              <td colspan="5"><strong>NON HAI INVIATO NESSUN MESSAGGIO</strong></td>
            </tr>
          <?php endif; ?>
          </tbody>
        </table>
        </div>
      </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Footer -->
    <?php include "footer.php"; ?>

  </body>
</html>
