<?php
require 'mongodb.inc.php';
function alert($msg) {
    echo "<script type='text/javascript'>alert('$msg');</script>";
}

if (isset($_POST['action']) and $_POST['action'] == "Inserisci valutazione") {

  require 'db.inc.php';

  try {
    session_start();
    $utenteValutatore = $_SESSION['email'];
    $utenteValutato = $_POST['EmailUtente'];
    $codTragitto = $_POST['CodTragitto'];
    $testo = $_POST['testo'];
    $data = date("y/m/d");
    $valutazione = $_POST['valutazione'];

    $sql = 'CALL inserisciValutazione(?, ?, ?, ?, ?, ?)';
    $stmt = $pdo -> prepare($sql);
    $stmt -> bindParam(1, $utenteValutatore);
    $stmt -> bindParam(2, $utenteValutato);
    $stmt -> bindParam(3, $codTragitto);
    $stmt -> bindParam(4, $testo);
    $stmt -> bindParam(5, $data);
    $stmt -> bindParam(6, $valutazione);

    $stmt->execute();
    $stmt->closeCursor();
      echo "<script>alert('Valutazione Inserita'); window.location = './storicoCarpooling.php';</script>";
  } catch (Exception $e) {
    echo "Errore : ".$e->getMessage();
    exit();

  }

  exit();
}
if (isset($_SESSION['email'])) {
$data=date("Y-m-d H:i:s");
$bulkWrite = new MongoDB\Driver\BulkWrite;
$doc = ['avviso' => 'effettuata valutazione utente',
  'utente valutatore' => $_SESSION['email'], 'data' => $data ,
  'utente valutato' => $utenteValutato];
$bulkWrite->insert($doc);
$manager->executeBulkWrite('epool.logEpool', $bulkWrite);
}
