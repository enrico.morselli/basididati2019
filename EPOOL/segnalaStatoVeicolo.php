<?php

function alert($msg) {
    echo "<script type='text/javascript'>alert('$msg');</script>";
}

if (isset($_POST['action']) and $_POST['action'] == "Invia") {

  require 'db.inc.php';

  try {
    session_start();
    $utenteSegnalatore = $_SESSION['email'];
    $testo = $_POST['testo'];
    $data = date("y/m/d");
    $titolo= $_POST['titolo'];
    $targaVeicolo= $_POST['TargaVeicolo'];

    $sql = 'CALL inserisciSegnalazione(?,?,?,?,?)';
    $stmt = $pdo -> prepare($sql);
    $stmt -> bindParam(1, $utenteSegnalatore);
    $stmt -> bindParam(2, $targaVeicolo);
    $stmt -> bindParam(3, $titolo);
    $stmt -> bindParam(4, $testo);
    $stmt -> bindParam(5, $data);

    $stmt->execute();
    $stmt->closeCursor();

  } catch (Exception $e) {
    echo "Errore : ".$e->getMessage();
    exit();
  }

  echo "<script>alert('La sua segnalazione è stata inviata, grazie per la collaborazione'); window.location = './index.php';</script>";
  exit();
}

if (isset($_SESSION['email'])) {
  $data=date("Y-m-d H:i:s");
  $bulkWrite = new MongoDB\Driver\BulkWrite;
  $doc = ['avviso' => 'segnalazione veicolo',
    'utente' => $_SESSION['email'], 'data' => $data, 'veicolo' => $TargaVeicolo ];
  $bulkWrite->insert($doc);
  $manager->executeBulkWrite('epool.logEpool', $bulkWrite);
}
