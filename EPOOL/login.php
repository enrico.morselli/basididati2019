<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <title>Welcome</title>
  </head>
<body>
<p>
<?php

if (isset($_POST['action']) && $_POST['action'] == 'ACCEDI') {
  // Connessione al DB
  require 'db.inc.php';
  // Query SQL
  try {
  $email = $_POST["email"];
  $password = $_POST["password"];
      $query = 'CALL checkLogin(?,?,@out)';
      $stmt = $pdo->prepare($query);
      $stmt->bindParam(1, $email );
      $stmt->bindParam(2, $password);
      $stmt->execute();
      $output = $pdo->query("select @out;")->fetch();
      $num = $output['@out'];

      if ($num == 0) {
        echo ("<script LANGUAGE='JavaScript'>
        window.alert('EMAIL O PASSWORD ERRATI.');
        window.location.href='index.php';
        </script>");
      } else {
         if ($num == 1) {
            $tipo = 'UD';
         } elseif ($num == 2) {
            $tipo = 'UP';
         } elseif ($num == 3) {
            $tipo = 'US';
         }
         session_start();
         $_SESSION['email'] = $email;
         $_SESSION['tipo'] = $tipo;
         header('location: index.php');
         exit();
      }
   } catch (PDOException $e) {
      $error = 'Errore login';
      echo $e->getMessage();
      #include 'error.html.php';
      exit();
   }
 catch(PDOException $e) {
   echo("[ERRORE] Esecuzione procedura non riuscita: ".$e->getMessage());
   exit();
 }

 //include $_SERVER['DOCUMENT_ROOT'] . 'welcome.html.php';
} else {
  header("Location: index.php");
}
 ?>

</p>
</body>
</html>
