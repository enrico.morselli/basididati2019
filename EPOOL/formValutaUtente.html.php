<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Valuta Utente</title>

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>
  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
    <!--inserimento header-->
    <?php include 'header.php'; ?>
    <div class="container" id="divContainer">
      <div class="modal-dialog">
        <div class="modal-content" style="background: rgba(0,0,0,0.5)">
          <div class="modal-header">
            <h4 class="modal-title text-left text-light">Valutazione <?php echo ($_POST['EmailUtente']); ?></h4>
          </div>
            <form action="valutaUtente.php" method="post">
              <div class="modal-body">
                <div class="form-group">
                  <label class="col-sm-12 text-left text-light font-italic" id="label">Voto (da 1 a 5): </label>
                  <div class="col-sm-10">
                    <input type="number" name="valutazione" min="1" max="5">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-12 text-left text-light font-italic" id="label">Testo: </label>
                  <div class="col-sm-12">
                    <textarea name="testo" class="form-control" id="inputBody" rows="8"></textarea>
                  </div>
                </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-danger pull-left" data-dismiss="modal" onclick="location.href = 'index.php'">Annulla</button>
                  <input type="hidden" name="CodTragitto" value="<?php echo ($_POST['CodTragitto']); ?>">
                  <input type="hidden" name="EmailUtente" value="<?php echo ($_POST['EmailUtente']); ?>">
                  <input type="submit" name="action" value="Inserisci valutazione" class="btn btn-outline-success"><i class="fa fa-arrow-circle-right fa-lg"></i>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <?php include "footer.php"; ?>

  </body>
</html>
