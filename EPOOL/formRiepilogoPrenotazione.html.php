<html>
<link rel="stylesheet" media="(min-width: 640px)" href="min-640px.css">

  <head>
    <title>Epool homepage</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0,
      maximum-scale=1.0, minimum-scale=1.0">
      <!-- fogli di stile -->
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <link rel="stylesheet" type="text/css" href="css/style.css">
  </head>

  <body style="background-image: url(image/background.jpg); background-position: center; background-repeat: no-repeat; background-size: cover; background-attachment: fixed">
  <!-- inseriemnto header -->
  <?php include 'header.php'; ?>
  <div class="container" id="divContainer">
    <h1 align="center">RIEPILOGO PRENOTAZIONE</h1>
    <div id="divR" align="center">
    <table class="table" id="tabInfo">

        <tr>
            <td><label for="CampoNote" id="label">CAMPO NOTE: </label></td>
            <td><?php echo $_POST['CampoNote']; ?></td>
        </tr>
        <tr>
            <td><label for="OraInizio" id="label">ORARIO INIZIO: </label></td>
            <td><?php
                   $sql='SELECT OraInizio FROM PRENOTAZIONE';
                   $stmt = $pdo -> prepare($sql);
                   $stmt -> execute();
                   $result = $stmt -> fetch();
                   echo $result['OraInizio'];
             ?></td>
        </tr>
        <tr>
            <td><label for="IndirizzoPartenza" id="label">INDIRIZZO PARTENZA: </label></td>
            <td><?php echo $_POST['IndirizzoPartenza']; ?></td>
        </tr>
        <tr>
            <td><label for="IndirizzoArrivo" id="label">INDIRIZZO ARRIVO: </label></td>
            <td><?php echo $_POST['IndirizzoArrivo']; ?></td>
        </tr>
        <tr>
            <td><label for="TargaVeicolo" id="label">Targa Veicolo: </label></td>
            <td><?php echo $_POST['TargaVeicolo']; ?></td>
        </tr>
    </table>
</div>

  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script><?php include "footer.php"; ?>

</body>
</html>
