<?php
//connessione al database mongodb
require 'mongodb.inc.php';
//script per la visualizzazione della classifica degli utenti con la media voti più alta
//connessione al db
require 'db.inc.php';
//funzione per creare finestra di alert javascript
function alert($msg) {
   echo "<script type='text/javascript'>alert('$msg');</script>";
}
//richiamiamo la stored procedure per ottenere la media degli utenti
try {
  //chiamata alla stored procedure
  $query = 'CALL classificaUtenti()';
  $stmt = $pdo -> prepare($query);
  if (!$stmt -> execute()) {
    echo "Errore nella query: " . $dbc -> error. ".";
  } else {
    $resp = $stmt -> fetchAll(PDO::FETCH_ASSOC);
  }
} catch (Exception $e) {
  echo "Errore: " . $e . ".";
  exit();
}
//include il form per mostrare gli utenti presenti nel db
include 'formClassifica.html.php';
